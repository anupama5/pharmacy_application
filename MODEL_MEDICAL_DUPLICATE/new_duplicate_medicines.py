import json
import pandas as pd


def duplicate_medicines(model_master_dict, rev_dict):
    # based on packaging, manufacturer- sorting the mrp- removing larger one...........
    new_med_dict = {}
    for item in rev_dict:
        if len(rev_dict[item]) > 1:
            med_dict = {}
            packaging_list = []
            for med_item in model_master_dict:
                if item == model_master_dict[med_item].get('name'):
                    if model_master_dict[med_item].get("packaging"):
                            packaging_list.append(model_master_dict[med_item].get("packaging"))
                            med_dict[med_item] = model_master_dict[med_item]

            package_list = list(set(packaging_list))
            if len(med_dict) > 1 and (len(package_list) < len(med_dict)):
                
                for package in package_list:
                    mrp = []
                    key_id = ''
                    mrp = [med_dict[i].get('mrp') for i in med_dict if med_dict[i].get('packaging') == package]
                    mrp.sort(reverse=True)
                    
                    for i in med_dict:
                        if med_dict[i].get('packaging') == package:
                            if med_dict[i].get('mrp') == mrp[0]:
                                key_id = i
                    del med_dict[key_id]
                new_med_dict = new_med_dict | med_dict
    return new_med_dict



def replace_strings_from_dict(model_master_dict):
    rev_dict = {}
    for item in model_master_dict:
        rev_dict.setdefault(model_master_dict[item].get('name'), set()).add(item)
    duplicate_dict= duplicate_medicines(model_master_dict, rev_dict)
    with open('duplicate_medicine_json.json', 'w') as file:
        json.dump(duplicate_dict, file)


def get_model_master_lowercase(model_master_dict, str_to_replace, str_to_pkg_replace):
    for item in model_master_dict:
        for key, value in str_to_replace.items():
            if model_master_dict[item].get('name'):
                model_master_dict[item]['name'] = model_master_dict[item].get('name').lower().strip().replace(key, value)
            if model_master_dict[item].get('manufacturer'):    
                model_master_dict[item]['manufacturer'] = model_master_dict[item].get('manufacturer').lower().strip().replace(key, value)
        for key, value in str_to_pkg_replace.items(): 
            if model_master_dict[item].get('packaging'):
                model_master_dict[item]['packaging'] = ("".join(model_master_dict[item].get("packaging").lower().replace(key, value).split()))
    replace_strings_from_dict(model_master_dict)

    

def main():
    str_to_replace = {' tablet': ' tab', ' tablets': ' tab', ' ointment': ' oint', ' capsule': ' cap', ' capsules': ' cap',
                      ' caps': ' cap', ' condoms': ' condom', ' diapers': ' diaper', ' syrup': ' syp', ' injection': ' inj',
                      ' &': ' and', ',': ' ', '.': '', '-': ' ', '  ': ' '}
    
    str_to_pkg_replace = {' tablet': ' tab', ' tablets': ' tab', ' ointment': ' oint', ' capsule': ' cap', ' capsules': ' cap',
                      ' caps': ' cap', ' condoms': ' condom', ' diapers': ' diaper', ' syrup': ' syp', ' injection': ' inj',
                      ' drops': ' drop', ' drp': ' drop', ' pc': ' pcs', ' piece': ' pcs', ' gm': ' gms', ' spra': ' spray', ' spry': ' spray', ' cre': ' cream', ' crea':' cream', ' cerm':' cream', '.': '', '-': ' ', '  ': ' '}
   
    with open('model_item_master.json') as model:
        model_master_list = json.load(model)
    model_master_dict = {item.get('id'): item for item in model_master_list}
    get_model_master_lowercase(model_master_dict, str_to_replace, str_to_pkg_replace)



if __name__ == '__main__':
    main()
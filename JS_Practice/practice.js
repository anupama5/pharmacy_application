
//20-03-2022    Assessment..........

const num = 10
const str = "hello"
const bool = true

console.log("\nType of num: "+typeof(num)+ "\n str: "+typeof(str)+"\n bool: "+typeof(bool))

console.log(`Type of num ${typeof(num)} with string interpolation`)
// converting numeric to string
console.log("\nType coversion: (number to string) "+typeof(String(num)))



//Even & odd numbers
const a = [1,2,3,4,5,6,7,8,9,10]

const even = a.map((item) => {
    if (item%2 === 0){
        return item
    }
    else {
        return "a"
    }
})
const odd = a.filter((item) => {
    if (item%2 !== 0){
        return item
    }
})

console.log("\n\nEven numbers: "+even)
console.log("Odd numbers: "+odd)

//Even prime & odd prime
const evenPrime = even.filter((item) => {
    for (let i = 2; i < item; i++){
        if(item % i == 0) {
            return false
        }
    }
    return item > 2
})
const oddPrime = odd.filter((item) => {
    for (let i = 2; i < item; i++){
        if(item % i == 0) {
            return false
        }
    }
    return item > 1
})

console.log("\n2 is not a prime/Composite\nEven prime numbers: "+evenPrime)
console.log("\nodd prime numbers: "+oddPrime)
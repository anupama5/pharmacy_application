class Hero {
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    greet() {
        return `${this.name} says hello.`
    }
}

const hero = new Hero('anupama', 3)
console.log(hero.greet())




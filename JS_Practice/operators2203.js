const arr = [10, 20, 12, 14, 15]

console.log(arr.length)
console.log(arr.indexOf(20))

arr[6] = 30
console.log(arr)

arr.push(5)
console.log(arr)

arr.unshift(1)
console.log(arr)

arr.splice(6,1)
console.log(arr)

arr.slice(0,5);
console.log(arr)


//for...of loop

const arr1 = [1,2,3,4,5,6]

for (let i of arr1) {
    console.log(i*i)
}

let arr2 = arr1.join('  ')
console.log(arr2)

//function......

const fname = {
    name: "anu",
    place: "TVM",
    age: "30",
    greet: function() {
        return `Hi, iam ${this.name}`
    },
};
console.log(fname.greet())
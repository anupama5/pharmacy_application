const fname = {
    name : 'Anupama',
    yearBirth : 2000,
    describe() {
        console.log(`I am ${this.name} `)
    },
}
fname.describe()

//.......
const lname = {
    name : 'Neena',
    yearBirth : 2001,
    details : {
        name: "ramesh",
        qualification : 'mca',
        job : 'Bank PO',
        moreDetails() {
            console.log(`This is more details about ${this.name} born in ${this.yearBirth}. worked as a ${this.job}`)
        },
    },
}
lname.details.moreDetails()

//
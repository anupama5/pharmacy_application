function square(x) {
    if( typeof x === 'undefined') {
        x = 5;
    }
    return x * x;
}
console.log(square());

const cube = x => {
    if (typeof x === 'undefined'){
        x = 6;
    }
    return x * x * x;
}
console.log(cube());

//Randome numbers

function randomNumbers() {
    return Math.floor(Math.random() * 10);
}
console.log(randomNumbers());

function cube1(x = randomNumbers()) {
    return x * x * x;
}

console.log(cube1());
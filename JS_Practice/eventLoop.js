
function first() {
    console.log(1);
}
function second() {
    setTimeout(() => {
    console.log(2)
}, 1)
}
function third() {
    console.log(3);
}
first()
second()
third()

// callback

function fun() {
    console.log("This is callback function testing.")
}

function getFun(callback) {
    callback()
}

getFun(fun);
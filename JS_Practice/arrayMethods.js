//filter()

const vehicle = [18, 20, 15, 10, 30];
const result = vehicle.filter(check);

function check(vehicle) {
 return vehicle >18;
}
console.log(result)

//2nd example

const filterAns = vehicle.filter((item) => {
    return item <=18
})
console.log(filterAns)

//map().......

const mapItem = vehicle.map((item) => {
    return item <=18
})
console.log(filterAns)

const items = [{name: "apple", price: 100},
{name: "coconut", price: 50},
{name: "milk", price: 10},
{name: "milk", price: 15}
]
const itemsName = items.map((item) => {
    return item.price
})
console.log(itemsName)

//find()

const itemNames = items.find((item) => {
    return item.name == "milk"
})

console.log(itemNames)

//forEach()
items.forEach((item) => {
    console.log(item.price);
})

//some()
const someFun = items.some((item) => {
    return item.price < 10
})
console.log(someFun)

//every()
const everyFun = items.every((item) => {
    return item.price >= 15
})
console.log(everyFun)
//reduce()


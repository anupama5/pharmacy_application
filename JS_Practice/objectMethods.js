//Initialize an object with properties and methods

const job = {
    position : 'cashier',
    type : 'hourly',
    isAvailable : true,

    showDetails() {
        const accepting = this.isAvailable ? 'is accepting application' : 'is currently accepting applications';

        console.log(`The ${this.position} position is ${this.type} and ${accepting}.`)
    }
};


// creating a new object...
const job1 = Object.create(job);

job1.showDetails();

job1.position = 'Accountant';
job1.showDetails();

//get the keys of an objects

const job2 = Object.keys(job);
console.log(job2);

//Get the keys and values of Object

const details = {
    fname : "anupama",
    lname : "anil",
    age : 30,
    location : "TVM"
};

const obj = Object.keys(details);
console.log(obj);

//iterating through the keys
Object.keys(details).forEach(item => {
    const itemvalue = details[item];
    console.log(`${item} : ${itemvalue}`);
});

//values()

const obj1 = Object.values(details);
console.log(obj1);

//entries.

const obj2 = Object.entries(details);
console.log(obj2);

//Assign... Intialize two objects 

const personal = {
    name : "neena",
    age : 26
};

const educational = {
    qualification : "BSc",
    yearOfPassout : 2019
};

const obj3 = Object.assign(personal,educational);
console.log(obj3);

const obj4 = Object.freeze(personal);
obj4.name = "Riya";
obj4.place = "Kollam";

console.log(obj4);

//seal().... can't add new properties but can change the existing proprties.

const obj5 = Object.seal(educational);
obj5.qualification = "MSc";
obj5.place = "Kollam";

console.log(obj5);

//get the internal hidden prototype
Object.getPrototypeOf(personal);

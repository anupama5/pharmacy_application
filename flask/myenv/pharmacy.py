
import json
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

pharma =[ {
            "pharmacyName" : "abc",
            "email" : "abc@gmail.com",
            "contact" : "12344",
            "address" : "gdfjdgfjdfg"
        },
                 {
            "pharmacyName" : "def",
            "email" : "abc@gmail.com",
            "contact" : "12344",
            "address" : "gdfjdgfjdfg"
        }]

@app.route('/', methods=['GET', 'POST'])
def pharmacy():
    
    # GET request
    if (request.method == 'GET'):
        return jsonify(pharma)
    
    if (request.method == 'POST'):
        data = request.get_json(force = True)
        #print(data)
        pharma.append(data)
        #res = json.dumps(data)
        print(pharma)
        return data

if __name__ == '__main__':
    app.run()
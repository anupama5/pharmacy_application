
// console.log(document.getElementById('head'))
function pharmacyDetails() {

    document.getElementById('details').style.display = "block";

    fetch('http://127.0.0.1:5000/', {method: "GET"}).then((data) => data.json()).then((pharmacyData) => {
        
        document.getElementById('tableView').innerHTML = pharmacyData.map(values => 
            `<tbody>
                <tr>
                <td>${values.pharmacyName}</td>
                <td>${values.email}</td>
                <td>${values.contact}</td>
                <td>${values.address}</td>
                </tr>
            </tbody>`).join('')
    }).catch((error) => {
        console.log(error);
    })
}

//Add the details of pharmacy
const eventId = document.getElementById('adding');

//optional chaining
eventId?.addEventListener("click", 
function addDetails(event) {
    console.log(document.getElementById('adding'))
    event.preventDefault();
    
    //Get the values using id selector
    const pharmacyName = document.getElementById('pharmacyName').value;
    const emailid = document.getElementById('emailid').value;
    const contact = document.getElementById('contact').value;
    const address = document.getElementById('address').value;

    //create an empty Object
    const pharmacyData = {};
    //create an empty Array
    const pharmaArray = [];

    //add the values into Object
    pharmacyData.pharmacyName = pharmacyName;
    pharmacyData.email = emailid;
    pharmacyData.contact = contact;
    pharmacyData.address = address;

    if (pharmacyName == "" || emailid == "" || contact == "" || address == "") {
        alert("Please fill the fields.");
    }
    else {
        pharmaArray.push(pharmacyName)
        pharmaArray.push(emailid)
        pharmaArray.push(contact)
        pharmaArray.push(address)

        console.log(pharmaArray);

        console.log(JSON.stringify(pharmacyData));

        div = fetch('http://127.0.0.1:5000/', {
            method: 'POST',
            body: JSON.stringify(pharmacyData),
            headers: {
                'Content-type': 'application/json;'
            }
        })
        .then(response => response.json())
        .then(json => {
            console.log(json);
        });

            document.getElementById("tableView").display='block';
            document.getElementById('tableView').innerHTML = 
                    `<thead id='tablehead'><tr><th>PHARMACY NAME</th><th>EMAIL ID</th><th>CONTACT NO.</th>
                    <th>ADDRESS</th></tr></thead><tbody>
                        <tr>
                        <td>${pharmaArray[0]}</td>
                        <td>${pharmaArray[1]}</td>
                        <td>${pharmaArray[2]}</td>
                        <td>${pharmaArray[3]}</td>
                        </tr>
                    </tbody>`;
    }
})


import argparse
import json
import datetime
import pandas as pd 



def writeToCSV_no_of_order_request(order_date_list, no_unattended_order, no_submitted_order, city):  
    order_dict = {'Date': order_date_list, 'City': city, 'No. of Unattended orders': no_unattended_order, 'No. of Submitted orders': no_submitted_order}  
    df = pd.DataFrame(order_dict) 
    df.to_csv('no_of_order_requests.csv', index = False)



def writeToCSV_orders(order_date_list, deliver_order, pending_order, reqpay_order, rej_cust_order, rej_pharm_order, order_amount, city):  
    order_dict = {'Date': order_date_list, 'City': city, 'Pending Orders' : pending_order, 'Delivered Orders': deliver_order, 'Request_Payment Orders': reqpay_order, 'Cancelled by Customer' :rej_cust_order, 'Cancelled by Shop' :rej_pharm_order, 'Order Amount' : order_amount}  
    df = pd.DataFrame(order_dict) 
    df.to_csv('no_of_orders.csv', index = False)



def order_amounts(citywise_order, day):
    sum_order_amount = 0
    for item in citywise_order:
        if str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day:
            if citywise_order[item].get('status') == 'delivered' or citywise_order[item].get('status') == 'pending' or citywise_order[item].get('status') == 'request_payment' or (citywise_order[item].get('status') == 'rejected' and citywise_order[item].get('cancelReason') == 'Cancelled by Customer') or (citywise_order[item].get('status') == 'rejected' and citywise_order[item].get('cancelReason') != 'Cancelled by Customer'):
                sum_order_amount = sum_order_amount + citywise_order[item].get('orderAmt')
                
    return sum_order_amount



def status_data_from_order(company_dict, order_dict, order_date, city):
    order_amount, order_date_list, deliver_order, pending_order, reqpay_order, rej_cust_order, rej_pharm_order = [], [], [], [], [], [], []
    citywise_order = {item : order_dict[item] for item in order_dict for company_item in company_dict if (order_dict[item].get('sellerId') == company_item)}

    for day in order_date:
        delivered_order_list = [item for item in citywise_order if citywise_order[item].get('status') == 'delivered' and str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day]
        pending_order_list = [item for item in citywise_order if citywise_order[item].get('status') == 'pending' and str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day]
        request_payment_order_list = [item for item in citywise_order if citywise_order[item].get('status') == 'request_payment' and str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day]
        rejected_cust_order_list = [item for item in citywise_order if citywise_order[item].get('status') == 'rejected' and citywise_order[item].get('cancelReason') == 'Cancelled by Customer' and str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day]
        rejected_pharm_order_list = [item for item in citywise_order if citywise_order[item].get('status') == 'rejected' and citywise_order[item].get('cancelReason') != 'Cancelled by Customer' and str(datetime.date.fromtimestamp(citywise_order[item].get('createdOn'))) == day]                
        
        order_amount.append(order_amounts(citywise_order, day))
        order_date_list.append(day)
        deliver_order.append(len(delivered_order_list))
        pending_order.append(len(pending_order_list))
        reqpay_order.append(len(request_payment_order_list))
        rej_cust_order.append(len(rejected_cust_order_list))
        rej_pharm_order.append(len(rejected_pharm_order_list))
    
    writeToCSV_orders(order_date_list, deliver_order, pending_order, reqpay_order, rej_cust_order, rej_pharm_order, order_amount, city)
    return True



def date_wise_no_of_order_request(order_unattended_status_dict, order_submitted_status_dict, order_date, city):
    order_date_list, no_unattended_order, no_submitted_order = [], [], []
    for day in order_date:
        unattended_order = [item for item in order_unattended_status_dict if str(datetime.date.fromtimestamp(order_unattended_status_dict[item].get('createdOn'))) == day]
        submitted_order = [item for item in order_submitted_status_dict if str(datetime.date.fromtimestamp(order_submitted_status_dict[item].get('createdOn'))) == day]
                
        order_date_list.append(day)
        no_unattended_order.append(len(unattended_order))
        no_submitted_order.append(len(submitted_order))
    writeToCSV_no_of_order_request(order_date_list, no_unattended_order, no_submitted_order, city)
    return True



def no_of_orders(company_dict, order_request_dict, order_date, city):
    order_unattended_status_dict = {item : order_request_dict[item] for item in order_request_dict for company_item in company_dict if (order_request_dict[item].get('cid') == company_item) and (order_request_dict[item].get('status') == 'unattended')}
    order_submitted_status_dict = {item : order_request_dict[item] for item in order_request_dict for company_item in company_dict if (order_request_dict[item].get('cid') == company_item) and (order_request_dict[item].get('status') == 'submitted')} 
    res = date_wise_no_of_order_request(order_unattended_status_dict, order_submitted_status_dict, order_date, city)
    return True



def json_list_files(in_file):
    with open(in_file) as file:
        return json.load(file) 



def converting_json_to_dict(company_jsonfiles, order_request_jsonfiles, order_jsonfiles, start_date, end_date, order_date, city):
    company_list = json_list_files(company_jsonfiles)
    order_request_list = json_list_files(order_request_jsonfiles)
    order_list = json_list_files(order_jsonfiles)

    company_dict = {item.get('id') : item for item in company_list if item.get('city') == city.capitalize()}
    order_request_dict = {item.get('id') : item for item in order_request_list if start_date <= item.get('createdOn') <= end_date}
    order_dict = {item.get('id') : item for item in order_list if (item.get('sellerId') and start_date <= item.get('createdOn') <= end_date)}
    
    no_of_order = no_of_orders(company_dict, order_request_dict, order_date, city)
    order = status_data_from_order(company_dict, order_dict, order_date, city)
    return True



def main_function(input_day, input_month, no_of_day, city):
    company_jsonfiles = "company.json"
    order_request_jsonfiles = "order_request.json"
    order_jsonfiles = "order.json"

    start_date = datetime.datetime(2022, input_month, input_day, 0, 0, 0).timestamp()
    end_date = datetime.datetime(2022, input_month, input_day, 23, 59, 59).timestamp() + (86400 * no_of_day)
    order_date = []
    for day in range(no_of_day):
        days = datetime.date.fromtimestamp(start_date + (86400 * day))
        order_date.append(str(days))

    no_orders = converting_json_to_dict(company_jsonfiles, order_request_jsonfiles, order_jsonfiles, int(start_date), int(end_date), order_date, city)
    return True



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_month', type=int, required=True)
    parser.add_argument('--start_day', type=int, required=True)
    parser.add_argument('--number_of_days', type=int, required=True)
    parser.add_argument('--city', type=str, required=True)

    args = parser.parse_args()

    input_day = args.start_day
    input_month = args.start_month
    no_of_day = args.number_of_days
    city = args.city
    result_data = main_function(input_day, input_month, no_of_day, city)
    return True



if __name__ == '__main__':
    main()

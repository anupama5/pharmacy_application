import json
import glob

### TASK 1 : merge 22 json files and extract medicine_name
files = glob.glob('evital*.json')
add_dict = {}
medicine_list = []

#for file in glob.glob('*.json'):
for file in files:
    with open(file, 'r') as infile:
        indict = json.load(infile)
    add_dict = add_dict | indict
dict = json.dumps(add_dict)
        
with open("finalJSON.json", 'w') as outfile:
    outfile.writelines(dict)
    
# Extract medicine name
for medicine in add_dict.values():
    if ((medicine['data'] != None) and (medicine['data']['medicine_name'] != None)):
        medicine_list.append(medicine['data']['medicine_name'])
        print(medicine['data']['medicine_name'])
print(len(medicine_list))
       
        

import json
import glob
import csv
from xlsxwriter import Workbook
from fuzzywuzzy import fuzz

c_name, company_name, manufacturer_list, manufacturer_name, match_company, match_manufacturer, match_manufacturer1, match_manufacturer2, match_manufacturer3, match_manufacturer4 = ([] for i in range(10))
add_dict = {}
m_count = 0
str_to_replace = {'ltd': 'limited', 'pvt': 'private', '(p)': 'private', '&': 'and', 'co.' : 'company', '.': ' ', '  ': ' ', ',': ' ', '-': ' '}
files = glob.glob('evital*.json')

### extract c_name from text file (in between c_code & c_user) & print the count

input_file = "items.txt"

with open(input_file, 'rt') as file:
    content = csv.reader(file, delimiter="\t")
    for row in content:
        c_name.append(row[5])
print('No.of company names :',len(c_name)-1)   

### merge 22 json files and extract medicine_name

for file in files:
    with open(file, 'r') as infile:
        indict = json.load(infile)
    add_dict = add_dict | indict
        
# Extract manufacturer_name
for manufacturer in add_dict.values():
    if ((manufacturer['data'] != None) and (manufacturer['data']['manufacturer_name'] != None)):
        manufacturer_list.append(manufacturer['data']['manufacturer_name'])
        m_count += 1
print('No.of manufacturer names :',m_count)    

### TASK :print the common data( grouped first) included in both c_name & manufacturer_name
c_name.pop(0)

## replace the strings with str_to_replace
for name in c_name:
    name = name.lower()
    for key, value in str_to_replace.items():
        name = name.strip()                     ## removing starting space & ending space
        name = name.replace(key, value)
    company_name.append(name)

for name in manufacturer_list:
    name = name.lower()
    for key, value in str_to_replace.items():
        name = name.strip()
        name = name.replace(key, value)
    manufacturer_name.append(name)

## Removing duplicates   
print('After removing repeated names')
c_name = list(set(company_name))
manufacturer_list = list(set(manufacturer_name))

print('No.of company names :',len(c_name),'  No.of manufacturer names :',len(manufacturer_list))

for company in c_name:
    for manufacturer in manufacturer_list:
        if(fuzz.token_sort_ratio(company,manufacturer) == 100):
            match_company.append(company)
            match_manufacturer.append(manufacturer)
            match_manufacturer1.append("")
            match_manufacturer2.append("")
            match_manufacturer3.append("")
## Extracting unmatching data from c_name & manufacturer_name in seperate lists            
unmatch_c = [i for i in c_name if i not in match_company]
unmatch_m = [i for i in manufacturer_list if i not in match_manufacturer]

for company in unmatch_c:
    for manufacturer in unmatch_m:
        if ((fuzz.token_sort_ratio(company,manufacturer) >= 95) and (fuzz.token_sort_ratio(company,manufacturer) < 100)):
            match_company.append(company)
            match_manufacturer.append("")
            match_manufacturer1.append(manufacturer)
            match_manufacturer2.append("")
            match_manufacturer3.append("")

c_name.clear()                ## Reusing the list
manufacturer_list.clear()
## Extracting unmatching data from c_name & manufacturer_name in seperate lists  
c_name = [i for i in unmatch_c if i not in match_company]
manufacturer_list = [i for i in unmatch_m if i not in match_manufacturer1]

for company in c_name:
    for manufacturer in manufacturer_list:
        if ((fuzz.token_sort_ratio(company,manufacturer) >= 90) and (fuzz.token_sort_ratio(company,manufacturer) < 95)):
            match_company.append(company)
            match_manufacturer.append("")
            match_manufacturer1.append("")
            match_manufacturer2.append(manufacturer)
            match_manufacturer3.append("")

unmatch_c.clear()           ## Reusing the list
unmatch_m.clear()
## Extracting unmatching data from c_name & manufacturer_name in seperate lists            
unmatch_c = [i for i in c_name if i not in match_company]
unmatch_m = [i for i in manufacturer_list if i not in match_manufacturer2]

for company in unmatch_c:
    for manufacturer in unmatch_m:
        if ((fuzz.token_sort_ratio(company,manufacturer) >= 85) and (fuzz.token_sort_ratio(company,manufacturer) < 90)):
            match_company.append(company)
            match_manufacturer.append("")
            match_manufacturer1.append("")
            match_manufacturer2.append("")
            match_manufacturer3.append(manufacturer)
            
# create excel sheets
workbook = Workbook('Normalized_data.xlsx')
w_sheet = workbook.add_worksheet()

w_sheet.write(0, 0, 'c_name')
w_sheet.write(0, 1, 'manufacturer_name(100%)')
w_sheet.write(0, 2, 'manufacturer_name(95%)')
w_sheet.write(0, 3, 'manufacturer_name(90%)')
w_sheet.write(0, 4, 'manufacturer_name(85%)')

# Write the column data.
w_sheet.write_column(1, 0, match_company)
w_sheet.write_column(1, 1, match_manufacturer)
w_sheet.write_column(1, 2, match_manufacturer1)
w_sheet.write_column(1, 3, match_manufacturer2)
w_sheet.write_column(1, 4, match_manufacturer3)

workbook.close()
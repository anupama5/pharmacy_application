import json
import glob
import csv
from xlsxwriter import Workbook
from fuzzywuzzy import fuzz

files = glob.glob('evital*.json')
add_dict = {}
medicine_list, c_name = ([] for i in range(2))
m_count = 0

# create excel sheets
workbook = Workbook('Book1.xlsx')
w_sheet = workbook.add_worksheet()
w_sheet1 = workbook.add_worksheet()

for file in files:
    with open(file, 'r') as infile:
        indict = json.load(infile)
    add_dict = add_dict | indict
        
# Extract medicine_name
for medicine in add_dict.values():
    if ((medicine['data'] != None) and (medicine['data']['medicine_name'] != None)):
        medicine_list.append(medicine['data']['medicine_name'])
        m_count += 1
print(m_count)    

input_file = "items.txt"

with open(input_file, 'rt') as file:
    content = csv.reader(file, delimiter="\t")
    for row in content:
        c_name.append(row[1])
print(len(c_name)-1)   

### TASK 6 : print the common data( grouped first) included in both c_name & medicine_name

c_name.pop(0)

for m_name in c_name:
    for m_list in medicine_list:
        if(fuzz.token_sort_ratio(m_name,m_list) > 80):
            print(m_name," - - - ",m_list)

# s_size = 30000

# snd= lambda c_name, s_size: [c_name[i:i+s_size] for i in range(0, len(c_name), s_size)]
# result1 = snd(c_name,s_size)

# lst= lambda medicine_list, s_size: [medicine_list[i:i+s_size] for i in range(0, len(medicine_list), s_size)]
# result = lst(medicine_list,s_size)

# for c_list in result1:
#     for i in c_list:
#         for m_list in result:
#             for j in m_list:
#                 if(fuzz.token_sort_ratio(i,j) > 90):
#                     print(i,"....",j)

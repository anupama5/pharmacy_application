
import json
import glob
import csv
from xlsxwriter import Workbook
from fuzzywuzzy import fuzz

c_name, manufacturer_list, match_company, match_manufacturer = ([] for i in range(4))
add_dict = {}
m_count = 0
files = glob.glob('evital*.json')

### TASK 7 : extract c_name from text file (in between c_code & c_user) & print the count

input_file = "items.txt"

with open(input_file, 'rt') as file:
    content = csv.reader(file, delimiter="\t")
    for row in content:
        c_name.append(row[5])
print('No.of company names :',len(c_name)-1)   

### TASK 8 : merge 22 json files and extract medicine_name

for file in files:
    with open(file, 'r') as infile:
        indict = json.load(infile)
    add_dict = add_dict | indict
        
# Extract manufacturer_name
for manufacturer in add_dict.values():
    if ((manufacturer['data'] != None) and (manufacturer['data']['manufacturer_name'] != None)):
        manufacturer_list.append(manufacturer['data']['manufacturer_name'])
        m_count += 1
print('No.of manufacturer names :',m_count)    

### TASK 9 : print the common data( grouped first) included in both c_name & manufacturer_name

c_name.pop(0)
print('After removing repeated names:')
c_name = list(set(c_name))
print('No.of company names :',len(c_name))
manufacturer_list = list(set(manufacturer_list))
print('No.of manufacturer names :',len(manufacturer_list))

# create excel sheets
workbook = Workbook('Book1.xlsx')
w_sheet = workbook.add_worksheet()

w_sheet.write(0, 0, 'c_name')
w_sheet.write(0, 1, 'manufacturer_name')

for company in c_name:
    for manufacturer in manufacturer_list:
        if(fuzz.token_sort_ratio(company,manufacturer) > 95):
            match_company.append(company)
            match_manufacturer.append(manufacturer)
            
unmatch_c = [i for i in c_name if i not in match_company]
unmatch_m = [i for i in manufacturer_list if i not in match_manufacturer]

for i in unmatch_c:
    if i not in unmatch_m:
        match_company.append(i)
        match_manufacturer.append("")
for i in unmatch_m:
    if i not in unmatch_c:
        match_company.append("")
        match_manufacturer.append(i)

# Write the column data.
w_sheet.write_column(1, 0, match_company)
w_sheet.write_column(1, 1, match_manufacturer)

workbook.close()
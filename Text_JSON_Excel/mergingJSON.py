
import json
import glob
import csv
from xlsxwriter import Workbook

### TASK 1 : merge 22 json files and extract medicine_name

files = glob.glob('evital*.json')
add_dict = {}
medicine_list = []
c_name = []
m_count = 0

for file in files:
    with open(file, 'r') as infile:
        indict = json.load(infile)
    add_dict = add_dict | indict
        
# Extract medicine_name
for medicine in add_dict.values():
    if ((medicine['data'] != None) and (medicine['data']['medicine_name'] != None)):
        medicine_list.append(medicine['data']['medicine_name'])
        m_count += 1
print(m_count)    


### TASK 2 : extract c_name from text file & print the count

input_file = "items.txt"

with open(input_file, 'rt') as file:
    content = csv.reader(file, delimiter="\t")
    for row in content:
        c_name.append(row[1])
print(len(c_name)-1)   


### TASK 3 : print the common data included both files (c_name & medicine_name)

def common_member(medicine_list, c_name):
    list_medicine = set(medicine_list)
    name_c = set(c_name)
 
    if (list_medicine & name_c):
        common_list = list(list_medicine & name_c)
        print(common_list)

common_member(medicine_list, c_name)


### TASK 4 : print medicine_name & c_name into a EXCEL SHEET( Book1.xlsx )

workbook = Workbook('Book1.xlsx')
w_sheet = workbook.add_worksheet()

# Write the column headers if required.
w_sheet.write(0, 1, 'medicine_name')

# Write the column data.
w_sheet.write_column(0, 0, c_name)
w_sheet.write_column(1, 1, medicine_list)

workbook.close()

### TASK 5 : Grouping the similar medicine_names (from medicine_name) and print it

result = []
l_words = []

medicine = input("Enter The Name: ")
words = medicine.split()

for word in words:
    l_words.append(word.lower())
    
for str in medicine_list:
    str = str.lower()
    temp_list = [word for word in l_words if word in str]
   
    if (len(temp_list) == len(l_words)):
        result.append(str)
        print(str)
        
### TASK 6 : print the common data( grouped first) included in both c_name & medicine_name
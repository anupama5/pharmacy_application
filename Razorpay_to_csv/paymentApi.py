import requests
from requests.structures import CaseInsensitiveDict
import json



### cash free payin API   here amount is in rupee
def cash_free(s_date,e_date):
    cashfree_list = []
    lastReturnId = 0
    new_results = True
    url = "https://api.pharmacyone.io/prod/cf_payments"
    headers = CaseInsensitiveDict()
    headers["session-token"] = "wantednote"
    while new_results: 
        headers["Content-Type"] = "application/json"
        ## convert dictionary data into string using json.dumps
        data = json.dumps({"startDate": s_date, "endDate": e_date,"lastReturnId" : lastReturnId})
        response = requests.post(url, headers=headers, data=data)
        cf_dict = response.json()
        if 'data' in cf_dict:
            new_results = cf_dict.get("data").get("payments", [])
            if 'lastReturnId' in cf_dict['data']:
                lastReturnId = cf_dict['data']['lastReturnId']
        cashfree_list.extend(new_results)
    cashfree_dict = {str(item['referenceId']) : item for item in cashfree_list}
    return cashfree_dict



# Razorpayin API  amount is in paise convert to rupee
def razorpay_in(s_date,e_date):
    razorpayin_list = []
    skip = 0
    new_results = True
    url = "https://api.pharmacyone.io/prod/rzp_transaction"
    headers = CaseInsensitiveDict()
    headers["session-token"] = "wantednote"
    while new_results: 
        params_dict = {'skip' : str(skip), 'from' : str(s_date), 'to' : str(e_date)}
        response = requests.get(url, params=params_dict, headers=headers)
        dict_data = response.json() 
        if 'data' in dict_data:
            new_results = dict_data.get("data").get("items", [])
        razorpayin_list.extend(new_results)
        skip = int(skip) + 100
    razorpayin_dict = {item['id'] : item for item in razorpayin_list}
    for item in razorpayin_dict:
        if razorpayin_dict[item].get('amount'):
            razorpayin_dict[item]['amount'] = str((razorpayin_dict[item].get('amount'))/100)
    return razorpayin_dict



def razorpay_out(s_date,e_date):
    razorpout_list = []
    skip = 0
    new_results = True
    url = "https://api.pharmacyone.io/prod/rzp_payout"
    headers = CaseInsensitiveDict()
    headers["session-token"] = "wantednote"
    while new_results: 
        params_dict = {'skip' : str(skip),'from' : str(s_date),'to' : str(e_date)}
        response = requests.get(url, params=params_dict, headers=headers)
        dict_data = response.json() 
        if 'data' in dict_data:
            new_results = dict_data.get("data").get("items", [])
        razorpout_list.extend(new_results)
        skip = int(skip) + 100
    razorpout_dict = {item['source']['notes']['id'] : item for item in razorpout_list if item.get('source') and item.get('source').get('notes') and item.get('source').get('notes').get('id')}
    for item in razorpout_dict:
        if razorpout_dict[item].get('source') and razorpout_dict[item].get('source').get('amount'):
            razorpout_dict[item]['source']['amount'] = (razorpout_dict[item].get('source').get('amount'))/100
    return razorpout_dict



def settlement(settle_id):
    settlement_list = []
    url = f"https://api.pharmacyone.io/prod/settlement_transaction/{settle_id}"
    headers = CaseInsensitiveDict()
    headers["session-token"] = "wantednote"
    response = requests.get(url, headers=headers)
    dict_data = response.json() 
    if 'data' in dict_data:
        new_results = dict_data.get("data").get("items", [])
    settlement_list.extend(new_results)
    settlement_dict = {item['id'] : item for item in settlement_list}
    return settlement_dict

import datetime
import pandas as pd 



# Write the summary in csv file
def writeToCSV_matching_summary(total_amount_list, summary_date_list):    
    summary_dict = {'date': summary_date_list, 'total_expense': total_amount_list}  
    df = pd.DataFrame(summary_dict) 
    df.to_csv('Summary_sheet.csv')
    summary = 'Successfully create summary sheet.'
    return summary



# Write the matching details into csv file
def writeToCSV_matching(match_id, pout_cid, pout_amount, pout_utr, settlement_id, m_date):    
    total_amount = 0
    total_amount_date = ''
    if m_date != []:
        for amount in pout_amount:
            total_amount = total_amount + amount
        total_amount_date = m_date[0]

        matching_dict = {'pout_id': match_id, 'Cid': pout_cid, 'Amount': pout_amount, 'utr': pout_utr, 'Settlement_id': settlement_id, 'Date': m_date}    
        df = pd.DataFrame(matching_dict) 
        df.to_csv('Matching_'+total_amount_date+'.csv') 
    return total_amount, total_amount_date



def extract_datewaise_matching_payment(payin_payout_matching, pout_days):
    total_amount_list, summary_date_list = [], [] 
    for day in pout_days:
        match_id = [item for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]    
        pout_cid = [payin_payout_matching[item].get('source').get('notes').get('cid') if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('notes') and payin_payout_matching[item].get('source').get('notes').get('cid')) else payin_payout_matching[item].get('cid') if payin_payout_matching[item].get('cid') else '' for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]    
        pout_amount = [payin_payout_matching[item].get('source').get('amount') if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('amount')) else payin_payout_matching[item].get('amount') if payin_payout_matching[item].get('amount') else '' for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]
        pout_utr = [payin_payout_matching[item].get('source').get('utr') if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('utr')) else payin_payout_matching[item].get('utr') if payin_payout_matching[item].get('utr') else '' for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]
        settlement_id = [payin_payout_matching[item].get('source').get('notes').get('settlementId') if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('notes') and payin_payout_matching[item].get('source').get('notes').get('settlementId')) else payin_payout_matching[item].get('settlementId') if payin_payout_matching[item].get('settlementId') else '' for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]
        m_date = [str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at')) else str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) if payin_payout_matching[item].get('createdOn') else '' for item in payin_payout_matching if (payin_payout_matching[item].get('source') and payin_payout_matching[item].get('source').get('created_at') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('source').get('created_at'))) == day) or (payin_payout_matching[item].get('createdOn') and str(datetime.date.fromtimestamp(payin_payout_matching[item].get('createdOn'))) == day)]
        
        total_amount, total_amount_date = writeToCSV_matching(match_id, pout_cid, pout_amount, pout_utr, settlement_id, m_date)
        total_amount_list.append(total_amount)
        summary_date_list.append(total_amount_date)
    summary = writeToCSV_matching_summary(total_amount_list, summary_date_list)
    return summary



# Unmatching payin details
def unmatching_razorpayin(unmatch_razorpayin):
    payin_id = [item for item in unmatch_razorpayin]
    payin_cid = [unmatch_razorpayin[item].get('notes').get('cid') if (unmatch_razorpayin[item].get('notes') and unmatch_razorpayin[item].get('notes').get('cid')) else unmatch_razorpayin[item].get('cid') if unmatch_razorpayin[item].get('cid') else '' for item in unmatch_razorpayin]
    payin_amount = [unmatch_razorpayin[item].get('amount') if unmatch_razorpayin[item].get('amount') else '' for item in unmatch_razorpayin]
    payin_utr = [unmatch_razorpayin[item].get('utr') if unmatch_razorpayin[item].get('utr') else '' for item in unmatch_razorpayin]
    payin_date = [str(datetime.date.fromtimestamp(unmatch_razorpayin[item].get('created_at'))) if unmatch_razorpayin[item].get('created_at') else unmatch_razorpayin[item].get('paymentTime') if unmatch_razorpayin[item].get('paymentTime') else '' for item in unmatch_razorpayin]
    return payin_id, payin_cid, payin_amount, payin_utr, payin_date



# Unmatching razorpayout
def unmatching_razorpayout(unmatch_razorpout):
    payout_id = [item for item in unmatch_razorpout]
    payout_cid = [unmatch_razorpout[item].get('source').get('notes').get('cid') if (unmatch_razorpout[item].get('source') and unmatch_razorpout[item].get('source').get('notes') and unmatch_razorpout[item].get('source').get('notes').get('cid')) else unmatch_razorpout[item].get('cid') if unmatch_razorpout[item].get('cid') else '' for item in unmatch_razorpout]
    payout_amount = [unmatch_razorpout[item].get('source').get('amount') if (unmatch_razorpout[item].get('source') and unmatch_razorpout[item].get('source').get('amount')) else unmatch_razorpout[item].get('amount') if unmatch_razorpout[item].get('amount') else '' for item in unmatch_razorpout]
    payout_utr = [unmatch_razorpout[item].get('source').get('utr') if (unmatch_razorpout[item].get('source') and unmatch_razorpout[item].get('source').get('utr')) else unmatch_razorpout[item].get('utr') if unmatch_razorpout[item].get('utr') else '' for item in unmatch_razorpout]
    payout_date = [str(datetime.date.fromtimestamp(unmatch_razorpout[item].get('source').get('created_at'))) if (unmatch_razorpout[item].get('source') and unmatch_razorpout[item].get('source').get('created_at')) else str(datetime.date.fromtimestamp(unmatch_razorpout[item].get('createdOn'))) if unmatch_razorpout[item].get('createdOn') else '' for item in unmatch_razorpout]
    return payout_id, payout_cid, payout_amount, payout_utr, payout_date



def writeToCSV_unmatching_payin(payin_id, payin_cid, payin_amount, payin_utr, payin_date):
    unmatching_payin_dict = {'payin_id': payin_id, 'Cid': payin_cid, 'Amount': payin_amount, 'utr': payin_utr, 'Date': payin_date}  
    df = pd.DataFrame(unmatching_payin_dict) 
    df.to_csv('Unmatching_Payin.csv') 
    result_msg = 'Successfully created.'
    return result_msg



def writeToCSV_unmatching_payout(payout_id, payout_cid, payout_amount, payout_utr, payout_date):
    unmatching_payout_dict = {'payin_id': payout_id, 'Cid': payout_cid, 'Amount': payout_amount, 'utr': payout_utr, 'Date': payout_date}  
    df = pd.DataFrame(unmatching_payout_dict) 
    df.to_csv('Unmatching_Payout.csv') 
    result_msg = 'Successfully created.'
    return result_msg



def extract_unmatching_payments(unmatch_razorpayin, unmatch_razorpout):
    payin_id, payin_cid, payin_amount, payin_utr, payin_date = unmatching_razorpayin(unmatch_razorpayin)
    payout_id, payout_cid, payout_amount, payout_utr, payout_date = unmatching_razorpayout(unmatch_razorpout)
    unmatch_payin = writeToCSV_unmatching_payin(payin_id, payin_cid, payin_amount, payin_utr, payin_date)
    unmatch_payout = writeToCSV_unmatching_payout(payout_id, payout_cid, payout_amount, payout_utr, payout_date)
    return unmatch_payin
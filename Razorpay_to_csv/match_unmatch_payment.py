from paymentApi import cash_free, razorpay_in, razorpay_out, settlement
from datewise_matching_payments import extract_datewaise_matching_payment,extract_unmatching_payments
import argparse
import datetime
import calendar


# Matching & unmatching payin & payout
def match_unmatch_dictionaries(razorpayin_dict, razorpout_dict):
    payin_payout_matching = {item : razorpout_dict[item] for item in razorpayin_dict if item in razorpout_dict}
    unmatch_razorpout = {item : razorpout_dict[item] for item in razorpout_dict if item not in razorpayin_dict}
    unmatch_razorpayin = {item : razorpayin_dict[item] for item in razorpayin_dict if item not in razorpout_dict}
    return payin_payout_matching, unmatch_razorpayin, unmatch_razorpout



def razorpayout_dict(payout_start_date, payout_end_date):
    res_settle_dict, res_razorpout_dict = {}, {}
    razorpout_dict = razorpay_out(payout_start_date, payout_end_date)
    # if we need the payments if status is 'processed'
    for item in razorpout_dict:
        if razorpout_dict[item].get('source').get('status') == 'processed':
            if razorpout_dict[item].get('source').get('notes').get('type') == 'settlement':
                settle_id = razorpout_dict[item].get('source').get('notes').get('id')
                settlement_dict = settlement(settle_id)
                res_settle_dict = res_settle_dict | settlement_dict
            else:
                res_razorpout_dict[item] = razorpout_dict[item]
    combine_razorpout_dict = res_razorpout_dict | res_settle_dict
    return combine_razorpout_dict



# datas from payin & payout API's
def data_from_paymentsAPI(payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days):
    cashfree_dict = cash_free(payin_start_date, payin_end_date)
    payin_dict = razorpay_in(int(payin_start_date), int(payin_end_date))
    razorpayin_dict = cashfree_dict | payin_dict

    razorpout_dict = razorpayout_dict(int(payout_start_date), int(payout_end_date))

    payin_payout_matching, unmatch_razorpayin, unmatch_razorpout = match_unmatch_dictionaries(razorpayin_dict, razorpout_dict)
    
    datewise_matching = extract_datewaise_matching_payment(payin_payout_matching, pout_days)
    unmatching_files = extract_unmatching_payments(unmatch_razorpayin, unmatch_razorpout)
    
    return unmatching_files

    

def check_payout_true_dates(input_day, input_month, no_of_day):
    pout_days = []
    no_of_prev_days = 2
    payin_start_date = datetime.datetime(2022, input_month, input_day, 0, 0, 0).timestamp()
    payout_start_date = datetime.datetime(2022, input_month, input_day, 0, 0, 0).timestamp() - (86400*2)
    payin_end_date = payout_end_date = datetime.datetime(2022, input_month, input_day, 23, 59, 59).timestamp() + (86400*(no_of_day-1))
    for day in range(no_of_day+2):
        days = datetime.date.fromtimestamp(payin_start_date - (86400*no_of_prev_days))
        pout_days.append(str(days))
        no_of_prev_days -= 1 
    return payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days



def check_payin_true_dates(input_day, input_month, no_of_day):
    pout_days = []
    payin_start_date = payout_start_date = datetime.datetime(2022, input_month, input_day, 0, 0, 0).timestamp()
    payin_end_date = datetime.datetime(2022, input_month, input_day, 23, 59, 59).timestamp() + (86400*(no_of_day-1))
    payout_end_date = datetime.datetime(2022, input_month, input_day, 23, 59, 59).timestamp() + (86400*(no_of_day+1))
    
    for day in range(no_of_day+2):
        days = datetime.date.fromtimestamp(payin_start_date + (86400*day))
        pout_days.append(str(days)) 
    return payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days



def check_payin_payout(input_day, input_month, no_of_day, check_payin, check_payout):

    if check_payin == True:
        payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days = check_payin_true_dates(input_day, input_month, no_of_day)
        matching_unmatching = data_from_paymentsAPI(payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days)
    elif check_payout == True:
        payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days = check_payout_true_dates(input_day, input_month, no_of_day)
        matching_unmatching = data_from_paymentsAPI(payin_start_date, payout_start_date, payin_end_date, payout_end_date, pout_days)
    return matching_unmatching



def main_function(input_day, input_month, no_of_day, check_payin, check_payout):
    last_date = calendar.monthrange(2022, input_month)[1]
    if (input_day <= last_date) and (input_month <= 12) and (no_of_day > 0):
        result_msg = check_payin_payout(input_day, input_month, no_of_day, check_payin, check_payout)
    else:
        result_msg = 'Enter Valid values.'
    return result_msg



if __name__ == '__main__':
    # User input --start_month, --start_day, --number_of_days ,--check_payin or check_payout
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_month', type=int, required=True)
    parser.add_argument('--start_day', type=int, required=True)
    parser.add_argument('--number_of_days', type=int, required=True)
    parser.add_argument('--check_payin',action='store_true')
    parser.add_argument('--check_payout',action='store_true')
    args = parser.parse_args()

    input_day = args.start_day
    input_month = args.start_month
    no_of_day = args.number_of_days
    check_payin = args.check_payin
    check_payout = args.check_payout

    result_msg = main_function(input_day, input_month, no_of_day, check_payin, check_payout)
    print(result_msg)

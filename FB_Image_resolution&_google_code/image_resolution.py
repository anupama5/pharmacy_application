# Import the Images module from pillow
from PIL import Image

image = Image.open("wskas3.jpg")
image = image.resize((600, 600))
image.save(fp="wskas3_cpy.jpg")

'''
{
    "from": "919845100004",
    "to": "917899397650",
    "content": {
        "header": {
            "type": "TEXT",
            "text": "1P Catalog"
        },
        "body": {
            "text": "*Baby Care*"
        },
        "action": {
            "catalogId": "505353217887413",
            "sections": [
                {
                    "title": "babycare",
                    "productRetailerIds": [
                        "idproduct43",
                        "idproduct44",
                        "idproduct45",
                        "idproduct46",
                        "idproduct47",
                        "idproduct48",
                        "idproduct49",
                        "idproduct50",
                        "idproduct51",
                        "idproduct52",
                        "idproduct53",
                        "idproduct54",
                        "idproduct55",
                        "idproduct56",
                        "idproduct57",
                        "idproduct58",
                        "idproduct59",
                        "idproduct60",
                        "idproduct61",
                        "idproduct62",
                        "idproduct63",
                        "idproduct64",
                        "idproduct65",
                        "idproduct66",
                        "idproduct67",
                        "idproduct68",
                        "idproduct69",
                        "idproduct70"
                    ]
                }
            ]
        }
    }
}
'''




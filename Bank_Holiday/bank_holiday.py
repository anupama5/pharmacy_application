### Give date and month as input. It will show the bank holidays.

import datetime
from datetime import date
import calendar

## Dictionary- Karnataka & UP & AP 2022 Bank holidays
holi_dict = { 'January': {
                '2022-01-01': 'New Year',
                '2022-01-14': 'Makar Sankranti / Pongal',
                '2022-01-15': 'Surya Pongal',
                '2022-01-26': 'Republic Day'
                },
            'February': {
                '2022-02-15': 'Hajarat Ali Birthday /UP'
                },
            'March': {
                '2022-03-01': 'Maha Shivratri',
                '2022-03-18': 'Holi'
                },
            'April': {
                '2022-04-01': 'Due to yearly closing of accounts, bank will be cosed.',
                '2022-04-02': 'Ugadi /AP & KA',
                '2022-04-05': 'Babu Jagjivan Ram Birthday /AP',
                '2022-04-10': 'Shri Ram Navami /UP',
                '2022-04-14': 'Mahaveera Jayanthi',
                '2022-04-15': 'Good Friday'
                },
            'May': {
                '2022-05-01': 'Labour Day',
                '2022-05-03': 'Basava Jayanthi/Khutba-e-Ramzan',
                '2022-05-16': 'Buddha Purnima /UP'
                },
            'June': {
                '2022-06-05': 'Dussehra /UP'
                },
            'July': {
                '2022-07-10': 'Id-ul-Adha',
                '2022-07-30': 'Last Day of Moharamm'
                },
            'August': {
                '2022-08-11': 'Raksha Bandhan /UP',
                '2022-08-15': 'Independence Day',
                '2022-08-19': 'Krishna Janmashtami /UP',
                '2022-08-31': 'Varasiddhi Vinayaka Vrata'
                },
            'October': {
                '2022-10-02': 'Gandhi Jayanti',
                '2022-10-04': 'Maha Navami',
                '2022-10-05': 'Vijaya Dasami',
                '2022-10-24': 'Diwali',
                '2022-10-26': 'Govardhan Puja /UP',
                '2022-10-27': 'Bhai Dooj /UP'
                },
            'November': {
                '2022-11-01': 'Kannada Rajyothsava',
                '2022-11-08': 'Guru Nanak Jayanti /UP',
                '2022-11-11': 'Kanakadasa Jayanthi'
                }
            }
try:
    # User input
    input_date = input("\nEnter Date: ")
    input_month = input("Enter Month: ")
    input_year = '2022'
    #input_year = input("Enter Year: ")
    input_day = input_date+' '+input_month+' '+input_year

    # finding the day
    def findDay(date):
        born = datetime.datetime.strptime(date, '%d %m %Y').weekday()
        return (calendar.day_name[born])

    # finding the day is holiday or not.
    def holiday(input_day):
        
        result = ""
        month = datetime.datetime.strptime(input_day, '%d %m %Y')
        full_month_name = month.strftime("%B")
        dt = datetime.datetime.strptime(input_day, '%d %m %Y').strftime('%Y-%m-%d')
        
        # iterating over the Statewise(KA, AP & UP) holidays
        for month, date_occasion in holi_dict.items():
            if (month == full_month_name):
                for date, occasion in date_occasion.items():
                    if(str(date) == dt):
                        result = "\n"+date+" - "+occasion+". Public holiday. Bank is not working."

        return result

    day = findDay(input_day)

    if(day == 'Sunday'):
        print("\nSunday Bank is not working.")

    elif(day == 'Saturday'):
        cal = calendar.monthcalendar(int(input_year), int(input_month))
        first_week  = cal[0]
        second_week = cal[1]
        third_week  = cal[2]
        fourth_week  = cal[3]
        
        if first_week[calendar.SATURDAY]:
            holi_day = second_week[calendar.SATURDAY]
        elif second_week[calendar.SATURDAY]:
            holi_day = third_week[calendar.SATURDAY]
        if fourth_week[calendar.SATURDAY]:
            fourth_day = fourth_week[calendar.SATURDAY]
            
        if(holi_day == int(input_date)):
                print("\nIts second Saturday. Bank is not working.")
        elif (fourth_day == int(input_date)):
                print("\nIts 4th Saturday. Bank is not working.")
        else:     
            result = holiday(input_day) 
            print(result) 
            if (result == ""): 
                print("Bank is working in this Saturday.")
            else:
                print(result) 
    else:
        result = holiday(input_day) 
        if (result == ""): 
            print("Bank is working.")
        else:
            print(result) 
        
except:
    print("Enter valid date.")        